-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 16-08-2018 a las 09:16:05
-- Versión del servidor: 10.1.34-MariaDB
-- Versión de PHP: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `colaboradores`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colaborador`
--

CREATE TABLE `colaborador` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `colaborador`
--

INSERT INTO `colaborador` (`id`, `nombre`) VALUES
(16, 'Bob Esponja'),
(17, 'Mario B'),
(18, 'Pocoyo'),
(19, 'Batman'),
(20, 'Falete'),
(21, 'Messi'),
(22, 'Sonic'),
(23, 'Mochilo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `colaborador_competencia`
--

CREATE TABLE `colaborador_competencia` (
  `colaborador` int(11) NOT NULL,
  `competencia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `colaborador_competencia`
--

INSERT INTO `colaborador_competencia` (`colaborador`, `competencia`) VALUES
(16, 1),
(16, 2),
(17, 1),
(17, 2),
(17, 3),
(18, 3),
(19, 1),
(20, 2),
(21, 1),
(21, 3),
(22, 1),
(22, 3),
(23, 1),
(23, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `competencia`
--

CREATE TABLE `competencia` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `categoria_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `competencia`
--

INSERT INTO `competencia` (`id`, `nombre`, `categoria_id`) VALUES
(1, 'css', 1),
(2, 'php', 2),
(3, 'java', 2),
(4, 'Android', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `competencia_cat`
--

CREATE TABLE `competencia_cat` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `competencia_cat`
--

INSERT INTO `competencia_cat` (`id`, `nombre`) VALUES
(1, 'diseño'),
(2, 'programacion');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `email`, `username`, `password`, `roles`) VALUES
(1, 'admin@admin.es', 'admin', '$2y$13$yNLdGNiky/QyaJHmwNcbtuNcos39WHUmQckvMaFwuuNkOG/OvnP4G', 'a:1:{i:0;s:9:\"ROLE_USER\";}'),
(2, 'prueba@gmail.com', 'prueba', '$2y$13$3GNbzwzDNSf3cb2rc90WIugVv9O91dbNNWYATZtTiqL1XcmO8D0ta', 'a:1:{i:0;s:9:\"ROLE_USER\";}'),
(3, 'prueba2@gmail.com', 'prueba2', '$2y$13$gZXhPYhdJ6GAdMxOq91Yj.bvppoh2oUS2TjSIgHFbGAGwJn7CCXpa', 'a:1:{i:0;s:9:\"ROLE_USER\";}'),
(4, 'prueba3@gmail.com', 'prueba3', '$2y$13$Zud3iyMTpv3AlEpYPK.BaO6qeGvrRI..7n6/YzDIEhv2/bicQ3yA6', 'a:1:{i:0;s:9:\"ROLE_USER\";}');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `colaborador`
--
ALTER TABLE `colaborador`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `colaborador_competencia`
--
ALTER TABLE `colaborador_competencia`
  ADD PRIMARY KEY (`colaborador`,`competencia`),
  ADD KEY `IDX_C3AA52F6D2F80BB3` (`colaborador`),
  ADD KEY `IDX_C3AA52F6842C498A` (`competencia`);

--
-- Indices de la tabla `competencia`
--
ALTER TABLE `competencia`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_842C498A3A909126` (`nombre`),
  ADD KEY `IDX_842C498A3397707A` (`categoria_id`);

--
-- Indices de la tabla `competencia_cat`
--
ALTER TABLE `competencia_cat`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_FD7E96D53A909126` (`nombre`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`),
  ADD UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `colaborador`
--
ALTER TABLE `colaborador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `competencia`
--
ALTER TABLE `competencia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `competencia_cat`
--
ALTER TABLE `competencia_cat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `colaborador_competencia`
--
ALTER TABLE `colaborador_competencia`
  ADD CONSTRAINT `FK_C3AA52F6842C498A` FOREIGN KEY (`competencia`) REFERENCES `competencia` (`id`),
  ADD CONSTRAINT `FK_C3AA52F6D2F80BB3` FOREIGN KEY (`colaborador`) REFERENCES `colaborador` (`id`);

--
-- Filtros para la tabla `competencia`
--
ALTER TABLE `competencia`
  ADD CONSTRAINT `FK_842C498A3397707A` FOREIGN KEY (`categoria_id`) REFERENCES `competencia_cat` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
