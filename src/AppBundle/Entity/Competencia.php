<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Competencia
 *
 * @ORM\Table(name="competencia")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CompetenciaRepository")
 */
class Competencia
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255, unique=true)
     */
    private $nombre;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ORM\ManyToMany(targetEntity="Colaborador", mappedBy="competencias")
     */
    private $colaborador;

    /**
     * @ORM\ManyToOne(targetEntity="CompetenciaCat", inversedBy="competencia")
     */
    private $categoria;

    public function __toString() {
        return $this->categoria;
    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Competencia
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->colaborador = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add colaborador
     *
     * @param \AppBundle\Entity\Colaborador $colaborador
     *
     * @return Competencia
     */
    public function addColaborador(\AppBundle\Entity\Colaborador $colaborador)
    {
        $this->colaborador[] = $colaborador;

        return $this;
    }

    /**
     * Remove colaborador
     *
     * @param \AppBundle\Entity\Colaborador $colaborador
     */
    public function removeColaborador(\AppBundle\Entity\Colaborador $colaborador)
    {
        $this->colaborador->removeElement($colaborador);
    }

    /**
     * Get colaborador
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getColaborador()
    {
        return $this->colaborador;
    }

    /**
     * Set categoria
     *
     * @param \AppBundle\Entity\CompetenciaCat $categoria
     *
     * @return Competencia
     */
    public function setCategoria(\AppBundle\Entity\CompetenciaCat $categoria = null)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return \AppBundle\Entity\CompetenciaCat
     */
    public function getCategoria()
    {
        return $this->categoria;
    }
}
