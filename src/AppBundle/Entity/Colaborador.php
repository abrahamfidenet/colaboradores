<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection; 

/**
 * Colaborador
 *
 * @ORM\Table(name="colaborador")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ColaboradorRepository")
 */
class Colaborador
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     */
    private $nombre;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection|Competencia[]
     *
     * @ORM\ManyToMany(targetEntity="Competencia", inversedBy="colaborador")
     * @ORM\JoinTable(name="colaborador_competencia",
     *     joinColumns={
     *      @ORM\JoinColumn(name="colaborador", referencedColumnName="id")
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(name="competencia", referencedColumnName="id")
     *  }
     *     )
     *
     *
     */
    private $competencias;

    public function __construct()
    {
        $this->competencias = new ArrayCollection();
    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Colaborador
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Add competencia
     *
     * @param \AppBundle\Entity\Competencia $competencia
     *
     * @return Colaborador
     */
    public function addCompetencia(\AppBundle\Entity\Competencia $competencia)
    {
        $this->competencias[] = $competencia;

        return $this;
    }

    /**
     * Remove competencia
     *
     * @param \AppBundle\Entity\Competencia $competencia
     */
    public function removeCompetencia(\AppBundle\Entity\Competencia $competencia)
    {
        $this->competencias->removeElement($competencia);
    }

    /**
     * Get competencias
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompetencias()
    {
        return $this->competencias;
    }
}
