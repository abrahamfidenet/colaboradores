<?php

namespace AppBundle\Repository;
use AppBundle\Entity\Colaborador;
/**
 * ColaboradorRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ColaboradorRepository extends \Doctrine\ORM\EntityRepository
{
	public function findByCompetencia($competencias)
    {
        dump($competencias);
        $qb = $this->createQueryBuilder("c")
            ->andWhere(':competencias MEMBER OF c.competencias')
            ->setParameters(array('competencias' => $competencias))
        ;
        return $qb->getQuery()->getResult();
    }

    public function findByCompetenciaAnd($competencias)
    {
        dump($competencias);
        $qb = $this->createQueryBuilder("c");
        $qb->select('c')
            ->where($qb->expr()->andX(
                ('c.competencias IN (:competencias)')
            ))
            ->setParameter('competencias', $competencias)
        ;
        return $qb->getQuery()->getResult();
    }

    public function findByNombreComp($name, $competencias)
    {
        $qb = $this->createQueryBuilder("c")
            ->where('c.nombre LIKE :name')
            ->andWhere(':competencias MEMBER OF c.competencias')
            ->setParameters(array(
                'competencias' => $competencias,
                'name' => $name))
        ;
        return $qb->getQuery()->getResult();

    }

    public function findAllQueryBuilder()
    {
        return $this->createQueryBuilder('colaborador');
    }
}
