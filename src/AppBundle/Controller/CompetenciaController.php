<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Competencia;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * competencia controller.
 *
 * @Route("user/competencia")
 */
class CompetenciaController extends Controller
{
    /**
     * Lists all competencia entities.
     *
     * @Route("/", name="competencia_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $paginator = $this->get('knp_paginator');

        $query = $em->getRepository('AppBundle:Competencia')->findAll();
        $pagination = $paginator->paginate($query, $request->query->getInt('page', 1), 5);

        return $this->render('competencia/index.html.twig', array(
            'competencias' => $pagination,

        ));
    }

    /**
     * Creates a new competencia entity.
     *
     * @Route("/new", name="competencia_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $competencia = new competencia();
        $form = $this->createForm('AppBundle\Form\CompetenciaType', $competencia);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($competencia);
            $em->flush();

            return $this->redirectToRoute('competencia_show', array('id' => $competencia->getId()));
        }

        return $this->render('competencia/new.html.twig', array(
            'competencia' => $competencia,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a competencia entity.
     *
     * @Route("/{id}", name="competencia_show")
     * @Method("GET")
     */
    public function showAction(Competencia $competencia)
    {
        $deleteForm = $this->createDeleteForm($competencia);

        return $this->render('competencia/show.html.twig', array(
            'competencia' => $competencia,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing competencia entity.
     *
     * @Route("/{id}/edit", name="competencia_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Competencia $competencia)
    {
        $deleteForm = $this->createDeleteForm($competencia);
        $editForm = $this->createForm('AppBundle\Form\CompetenciaType', $competencia);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('competencia_edit', array('id' => $competencia->getId()));
        }

        return $this->render('competencia/edit.html.twig', array(
            'competencia' => $competencia,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a competencia entity.
     *
     * @Route("/{id}", name="competencia_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Competencia $competencia)
    {
        $form = $this->createDeleteForm($competencia);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($competencia);
            $em->flush();
        }

        return $this->redirectToRoute('competencia_index');
    }

    /**
     * Creates a form to delete a competencia entity.
     *
     * @param Competencia $competencia The competencia entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Competencia $competencia)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('competencia_delete', array('id' => $competencia->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
