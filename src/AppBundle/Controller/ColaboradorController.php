<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Colaborador;
use AppBundle\Entity\Competencia;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Services\Helpers;


/**
 * Colaborador controller.
 *
 * @Route("user/colaborador")
 */
class ColaboradorController extends Controller
{
    /**
     * Lists all colaborador entities.
     *
     * @Route("/pag", name="colaborador_pag")
     * @Method({"GET", "POST"})
     */
    public function pagAction(Request $request)
    {
        $competenciasI = new Colaborador();
        $em = $this->getDoctrine()->getManager();
        $colaboradors = $em->getRepository('AppBundle:Colaborador')->findAll();
        $form = $this->createForm('AppBundle\Form\SearchType', $competenciasI);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate($colaboradors, $request->query->getInt('page', 1), 6);
        return $this->render('colaborador/pag.html.twig', array(
            'colaboradors' => $pagination,
            'form' => $form->createView(),

        ));

    }
    /**
     * Lists all colaborador entities.
     *
     * @Route("/", name="colaborador_index")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {

        $competenciasI = new Colaborador();
        $form = $this->createForm('AppBundle\Form\SearchType', $competenciasI);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        $paginator = $this->get('knp_paginator');

        if ($form->isSubmitted() && $form->isValid())
        {

            $competencias = $form->get("competencias")->getData();
            $name = $form->get("nombre")->getData();

            $repository = $em->getRepository('AppBundle:Colaborador');
            if ($competencias->isEmpty())
            {
                $query = $repository->findBy(array('nombre' => $name));
            }
            elseif ($name == "")
            {
                $query = $repository->findByCompetencia($competencias);
            }
            else {
                $query = $repository->findByNombreComp($name, $competencias);

            }
            /*** La key del paginator se establece a 1 en las buquedas para que no devuelva una página en la que no hay resultados*/
            $pagination = $paginator->paginate($query, $request->query->getInt(1, 1), 5);
            return $this->render('colaborador/index.html.twig', array(
            'results' => $pagination,
            'form' => $form->createView(), 
            ));
        }

        $query = $em->getRepository('AppBundle:Colaborador')->findAll();
        $pagination = $paginator->paginate($query, $request->query->getInt('page', 1), 5);
        return $this->render('colaborador/index.html.twig', array(
            'results' => $pagination,
            'form' => $form->createView(),
            
        ));
    }
    /**
     * Creates a new colaborador entity.
     *
     * @Route("/new", name="colaborador_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $colaborador = new Colaborador();
        $form = $this->createForm('AppBundle\Form\ColaboradorType', $colaborador);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($colaborador);
            $em->flush();

            return $this->redirectToRoute('colaborador_show', array('id' => $colaborador->getId()));
        }
        dump($colaborador);
        return $this->render('colaborador/new.html.twig', array(
            'colaborador' => $colaborador,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a colaborador entity.
     *
     * @Route("/{id}", name="colaborador_show")
     * @Method("GET")
     */
    public function showAction(Colaborador $colaborador)
    {
        $deleteForm = $this->createDeleteForm($colaborador);
        dump($colaborador);
        return $this->render('colaborador/show.html.twig', array(
            'colaborador' => $colaborador,

            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing colaborador entity.
     *
     * @Route("/{id}/edit", name="colaborador_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Colaborador $colaborador)
    {
        $deleteForm = $this->createDeleteForm($colaborador);
        $editForm = $this->createForm('AppBundle\Form\ColaboradorType', $colaborador);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('colaborador_edit', array('id' => $colaborador->getId()));
        }

        return $this->render('colaborador/edit.html.twig', array(
            'colaborador' => $colaborador,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a colaborador entity.
     *
     * @Route("/{id}", name="colaborador_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Colaborador $colaborador)
    {
        $form = $this->createDeleteForm($colaborador);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($colaborador);
            $em->flush();
        }

        return $this->redirectToRoute('colaborador_index');
    }

    /**
     * Creates a form to delete a colaborador entity.
     *
     * @param Colaborador $colaborador The colaborador entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Colaborador $colaborador)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('colaborador_delete', array('id' => $colaborador->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

}
