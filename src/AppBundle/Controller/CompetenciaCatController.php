<?php

namespace AppBundle\Controller;

use AppBundle\Entity\CompetenciaCat;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Competenciacat controller.
 *
 * @Route("user/competenciacat")
 */
class CompetenciaCatController extends Controller
{
    /**
     * Lists all competenciaCat entities.
     *
     * @Route("/", name="competenciacat_index")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $paginator = $this->get('knp_paginator');

        $query = $em->getRepository('AppBundle:CompetenciaCat')->findAll();
        $pagination = $paginator->paginate($query, $request->query->getInt('page', 1), 5);

        return $this->render('competenciacat/index.html.twig', array(
            'competenciaCats' => $pagination,
        ));
    }

    /**
     * Creates a new competenciaCat entity.
     *
     * @Route("/new", name="competenciacat_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $competenciaCat = new Competenciacat();
        $form = $this->createForm('AppBundle\Form\CompetenciaCatType', $competenciaCat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($competenciaCat);
            $em->flush();

            return $this->redirectToRoute('competenciacat_show', array('id' => $competenciaCat->getId()));
        }

        return $this->render('competenciacat/new.html.twig', array(
            'competenciaCat' => $competenciaCat,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a competenciaCat entity.
     *
     * @Route("/{id}", name="competenciacat_show")
     * @Method("GET")
     */
    public function showAction(CompetenciaCat $competenciaCat)
    {
        $deleteForm = $this->createDeleteForm($competenciaCat);

        return $this->render('competenciacat/show.html.twig', array(
            'competenciaCat' => $competenciaCat,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing competenciaCat entity.
     *
     * @Route("/{id}/edit", name="competenciacat_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, CompetenciaCat $competenciaCat)
    {
        $deleteForm = $this->createDeleteForm($competenciaCat);
        $editForm = $this->createForm('AppBundle\Form\CompetenciaCatType', $competenciaCat);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('competenciacat_edit', array('id' => $competenciaCat->getId()));
        }

        return $this->render('competenciacat/edit.html.twig', array(
            'competenciaCat' => $competenciaCat,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a competenciaCat entity.
     *
     * @Route("/{id}", name="competenciacat_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, CompetenciaCat $competenciaCat)
    {
        $form = $this->createDeleteForm($competenciaCat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($competenciaCat);
            $em->flush();
        }

        return $this->redirectToRoute('competenciacat_index');
    }

    /**
     * Creates a form to delete a competenciaCat entity.
     *
     * @param CompetenciaCat $competenciaCat The competenciaCat entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(CompetenciaCat $competenciaCat)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('competenciacat_delete', array('id' => $competenciaCat->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
